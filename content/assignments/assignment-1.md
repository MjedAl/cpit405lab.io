---
title: "Assignment 1"
date: 2018-08-25T22:54:32+03:00
draft: true
---
## Assignment 1: HTML
<span class="tag is-info is-medium">Due date: Tuesday 25/9/2018 at 12:00PM </span>

Implement three web pages for the image/photo hosting service or social network service of your choice. The web pages should be implemented in pure HTML as follows:

- Home/Landing page that shows a list of images.
- One page that has a form to search for images and filter results.
- One page that has a form to submit/upload a photo or do advance search.

The assignment should be stored in a repository on GitHub and hosted on [GitHub Pages](https://pages.github.com/) as a project site.  The repo may be named `cpit405-assignment-1`, so the URL looks like: `username.gitlab.io/cpit405-assignment-1`.

### Submission

Please submit the links to both your repository on GitHub and the hosted site on GitHub pages in private to the instructor on Slack .