---
title: "Assignment 3"
date: 2018-08-25T22:54:38+03:00
draft: true
---
## Assignment 3: JS
<span class="tag is-info is-medium">Due date: Monday 5/11/2018 at 12:00PM </span>

You are asked to sort a list of items (images) in JavaScript using some sort criteria. For example, images can be sorted by popularity (number of likes), date published (newest first), or the number of comments.

### Notes:

- You are not allowed to use a third-party library to sort an array (i.e., no _lodash_ or any other third-party library).
- The sort function has to be done at the client side (i.e. no server side code).

The assignment should be stored in a repository on GitHub and hosted on [GitHub Pages](https://pages.github.com/) as a project site.  The repo may be named `cpit405-assignment-3`, so the URL looks like: `username.gitlab.io/cpit405-assignment-3`.

### Submission

Please submit the links to both your repository on GitHub and the hosted site on GitHub pages in private to the instructor on Slack.