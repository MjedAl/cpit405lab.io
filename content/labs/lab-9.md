---
title: "Lab 9"
date: 2018-09-01T16:39:15+03:00
draft: false
---

# Lab 09: PHP and MariaDB

## Objective

-   PHP fundamentals

-   Variable and concatenate operator

-   Arithmetic operation

-   If-else statements, switch, and loop

-   Function, variable scopes, data structure (array and dictionary).

-   Class and OOP.

-   Code reuse and library concepts

-   Read and write files.

-   HTML form handler as server-side

-   Cookie, session, and file upload.

-   Database connection.

-   Retrieve/update/delete DB records

## Current Lab Learning Outcomes (LLO)

By completion of the lab the students should be able to

1.  Implement Server-Side script to serve client-side requests

2.  Develop dynamic web pages using Ajax technology

## Lab Requirements

Web Browser, Web Browser developer tools, Apache, PHP, MariaDB, Text
Editor, and PHP reference.

## Lab Assessment

1. Implement power function which takes two parameters a return the result. You may use recursive function or iterative.
2. Translate following diagram with demo code:
  <p>![](/images/labs/lab-8-pic-1.png)</p>
3. Create simple connection to database and retrieve all databases names using following SQL query:
  ```
SHOW DATABASES;
  ```
  

## Homework <span class="tag is-info is-medium">Due date week 11</span>

All HTML files that end with (.html) should change their extension into
(.php). Any form in your project should change its action to point to
file within **controllers** folder. Any communication to database
whether read or write should within model file where under **models**
folder. Each controller PHP file will process form request and may use
files in models to do changes to database.

Controller file handle any request such as update or delete. It will use
proper model file to update database or delete from database. Model file
handle any connection to database whether read, edit, or delete. Each
model file should handle read/update/delete information from one table.
Make sure you organize your code either using OOP, or functions.

Database should contain all related tables to your project. For example,
if you have list of products, those products should be stored in
database. Never list products or repeated information in HTML pages.
That information should be stored in proper database table. Finally,
make sure you create copy of your database creation and sample data
within a SQL file.

Extra credit, using Object-Oriented programming including class diagram,
and database Er-diagram.

## Lab Description

This exercise is individual work. The answer should be written as text
files with (.php) extension then submit including student ID and Name as
a comment in beginning of the PHP file.

### Homework
Make sure you follow following file structure

```sh
id_first-name-cpit405-lab-assignment3    # Root folder
   - views                               # sub-folder
       - page1.php
       - page2.php
       - page3.php
       - ...
   - css                                 # sub-folder
       - style1.css
       - style2.css
       - ...
   - js                                  # sub-folder
       - script1.js
       - script2.js
       - ...
    - controllers                        # sub-folder
       - controller1.php
       - controller2.php
       - ...
    - models                             # sub-folder
       - model1.php
       - model2.php
       - ...
   - Documents                           # sub-folder
     - db-install-statements.sql         # Must be SQL format
     - project-sketch.pdf                # Must be PDF format
     - project-description.pdf           # Must be PDF format
     
```

<span id="page2" class="anchor"></span>
