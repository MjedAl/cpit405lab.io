---
title: "Lab 4"
date: 2018-09-01T16:39:02+03:00
draft: false
---

# Lab 04: CSS Structure and Properties

## Objective

-   Use inline, embedded, and external CSS.

-   CSS selectors, Pseudocode selector, and priority

-   Practice mostly used CSS rules.

-   HTML/CSS layout design principle

-   Responsive design and media target

## Current Lab Learning Outcomes (LLO)

By completion of the lab the students should be able to

1.  Use HTML5 markup tags for structuring web pages

2.  Identify the compatibility issues between the well-known browsers

3.  Use HTML5 with appropriate CSS properties and elements for styling,
    > formatting, and enhancing web pages

4.  Construct and validate web pages using HTML5 and CSS3

## Lab Requirements

Web Browser, Web Browser developer tools, Text Editor, and HTML, and CSS
references.

## Lab Assessment

1.  Write css code using (.image-preview) class and descendant selectors to select `<img>` element and change its properties to match following figure.
 ```html
  <div class="image-preview">
    <img src='kau-logo.png'>
  </div>
 ```
 ![](/images/labs/lab-4-pic-1.png)

2.  For the following CSS code fill the table below
 ```html
<div id="header">
  <span class="myclass1"\>Every\</span>
  <span class="myclass2"\>one can\</span>
  <span class="myclass3"\>success.\</span>
</div>
<p>
  One machine can do the work of fifty ordinary men.
  <span class="mycalss4">No machine can do the work of one extraordinary man.</span>
</p>
<style>
    p > span {color: red; }
    #header span {color: pink; }
    span { color: green; }
    #header \*:last-child { color: orange; }
    #header span:first-child:hover { color: blue; }
</style>
 ```
|                    |   color   |   color when mouse over  |
|--------------------|-----------|--------------------------|
|  **span.myclass1** |           |                          |
|  **span.myclass2** |           |                          |         
|  **span.myclass3** |           |                          |      
|  **span.myclass4** |           |                          |

## Lab Description

This exercise is individual work. The answer for question 1 should be
written as text files with (.html) and (.css) extensions compressed
together then submit including student ID and Name as a comment in
beginning of the CSS file.

<span id="page2" class="anchor"></span>
