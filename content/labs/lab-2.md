---
title: "Lab 2"
date: 2018-09-01T16:38:51+03:00
draft: false
---

# Lab 02: Introduction, HTML Structure

## Objective

-   Prepared Web Development environment by installing required services
    such Apache, PHP, MariaDB, Web Browser developer tools and Text
    Editor.

-   Realize web site major parties to get started.

-   Practice HTML minimum structure and the different between document
    sections (header, and body).

-   Practice mostly used HTML tags.

-   Learn how to build HTML form, table, bullet/numbering/multilevel
    lists.

-   HTML layout design principle

## Current Lab Learning Outcomes (LLO)

By completion of the lab the students should be able to
1.  Understand the basic structure of the World-Wide-Web
2.  Use HTML5 markup tags for structuring web pages
3.  Identify the compatibility issues between the well-known browsers

## Lab Requirements

AAMP bundle application (Apache, PHP, MariaDB), Web Browser, Web Browser
developer tools, Text Editor, and HTML reference.

## Lab Assessment

1.  Transform the text below into a link that goes to “www.kau.edu.sa”.
 ```html
  <!DOCTYPE html>
     <html>
     <body>
     
     King Abdulaziz University
     
     </body>
     </html>
 ```
2. Mark up the following text with appropriate tags and complete missing tags:
  
  - "Universal Studios Presents" is the document title.
  
  - "Jurassic Park" is the page title
  
  - "About" is sub-title of Jurassic Park.
  
  - The last sentence is just a paragraph.
  ```html
  <!DOCTYPE html>
  <html>
    Universal Studios Presents
    <body>
      Jurassic Park
            About
      On the Island of Isla Nublar, a new park has been built: Jurassic Park is a theme park of cloned dinosaurs!!
  </body>
  </html>
  ```

## Homework (Due date week 5)

In this assignment, you need to prepare your lab project. First, you
need to List all pages’ titles. Second, describe each page propose.
Third, draw each page layout with details (*sketch*), finally, design
each page using HTML and CSS with same layout.

## Projects Checklist

| #    | Requirements                                                      |    Mark |
|------|-------------------------------------------------------------------|---------|
| 1    |   Several different data entries                                  |         |     
| 2    |    Several different data retrievals                              |         |     
| 3    |    Support session and login side and public side                 |         |     
| 4    |    Customer Registration                                          |         |   
| 5    |    Dynamic database                                               |         | 
| 7    |    Using PHP as server-side programming language and MariaDB as Database  | | 
 


## Lab Description

This exercise is individual work. The answer should be written as text
file with (.html) extension then submit including student ID and Name as
a comment in beginning of the html file.

### Homework
Make sure you follow following file structure

```sh
id_first-name-cpit405-lab-assignment2    # Root folder
   - views                               # sub-folder
       - page1.html
       - page2.html
       - page3.html
       - ...
   - css                                 # sub-folder
       - style1.css
       - style2.css
       - ...
   - Documents                           # sub-folder
     - project-sketch.pdf                # Must be PDF format
     - project-description.pdf           # Must be PDF format
     
```
