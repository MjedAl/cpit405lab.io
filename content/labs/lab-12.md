---
title: "Lab 12"
date: 2018-09-01T16:39:22+03:00
draft: false
---

# Lab 12: Ajax (DOM, XML, JSON, PHP)

## Objective

-   Ajax principle and when it is needed to be used and when is not.

-   Request and parse XML data/files

-   Generate XML data using PHP and retrieve it by Ajax

-   Request and parse JSON data

-   Generate JSON data using PHP and retrieve it by Ajax

-   Send POST request via Ajax

## Current Lab Learning Outcomes (LLO)

By completion of the lab the students should be able to

1.  Implement Server-Side script to serve client-side requests

2.  Develop dynamic web pages using Ajax technology

## Lab Requirements

Web Browser, Web Browser developer tools, Apache, PHP, MariaDB, and Text
Editor

## Lab Assessment

1.  Use PHP to store below Table data as array with keys.
2.  Translate the table below into JSON. *- Hints: You may use PHP JSON encode method `json_encode`*

    <table class="table is-bordered">
      <thead>
        <tr>
          <th>Name</th>
          <th>Total</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Mohammad</td>
          <td>4</td>
        </tr>
        <tr>
          <td>Fahad</td>
          <td>3.5</td>
        </tr>
      </tbody>
    </table>

3.  Use Ajax to requests PHP script using JSON format and display via Console.


## Homework <span class="tag is-info is-medium">Due date week 12</span>

Choose one part or your project to handle its contents using Ajax. The
process will be as following: a JS event for example (click) to call a
method that will request data from server using Ajax. The server handler
(a PHP file) where located in **controller** folder will process Ajax
parameters and call method from file within **model** folder that will
retrieve or update data from/in database. This method should return
array as request result.

So, the handler in **controller** will process the array and convert it
into XML or JSON and print it out as request result. When the request
finish from JS side it will process the request (either parsing XML or
JSON) and use this information to fill related fields or remove some
elements from the page when the Ajax request success; otherwise, it will
display message state that the request failed.

## Lab Description

This exercise is individual work. The answer should be written as text
files with (.html), (.js) and (.php) extensions then submit including
student ID and Name as a comment in beginning of the JS file.

### Homework
Make sure you follow following file structure

```sh
id_first-name-cpit405-lab-assignment3    # Root folder
   - views                               # sub-folder
       - page1.php
       - page2.php
       - page3.php
       - ...
   - css                                 # sub-folder
       - style1.css
       - style2.css
       - ...
   - js                                  # sub-folder
       - script1.js
       - script2.js
       - ...
    - controllers                        # sub-folder
       - controller1.php
       - controller2.php
       - ...
    - models                             # sub-folder
       - model1.php
       - model2.php
       - ...
   - Documents                           # sub-folder
     - db-install-statements.sql         # Must be SQL format
     - project-sketch.pdf                # Must be PDF format
     - project-description.pdf           # Must be PDF format
     
```

<span id="page2" class="anchor"></span>
