---
title: "Lab 7"
date: 2018-09-01T16:39:08+03:00
draft: false
---

# Lab 07: JavaScript

## Objective

-   Internal JS and debugging methods.

-   Variable and automatic type conversion

-   Arithmetic operation

-   If-else statements, switch, and loop

-   Popup windows (alert, prompt, and confirm)

-   Function, variable scopes, data structure (array, and stack). Modify
    actual language behavioral

-   Object, class and OOP.

-   DOM: interacting with document contents

-   Import external JS file

-   Event handling

-   Multi-threading concept in JavaScript

-   HTML5 Canvas for animation and draw.

## Current Lab Learning Outcomes (LLO)

By completion of the lab the students should be able to

1.  Identify the compatibility issues between the well-known browsers

2.  Implement client-side application logic using JavaScript

3.  Handle event using JavaScript and DOM in client-side.

## Lab Requirements

Web Browser, Web Browser developer tools, Text Editor, and JavaScript reference.

## Lab Assessment

1.  Write minimum HTML structure which include JavaScript code within the page. Show an alert displaying your Name.

2.  Write JS function that calculate factorial of *n*, implement a sample code to test this function. ***example: factorial(4) = 1 \* 2 \* 3 \* 4 = 24***

3.  Write JS code which a person class contains (*first\_name, last\_name, year\_of\_birth*) as class properties or attributes and (*getAge*(), *getFullName*()) as methods. *getAge* calculate the age that subtract *year\_of\_birth* from current year. *getFullName* combine first and last name and return them. Implement a sample object to test this class.

## Homework <span class="tag is-info is-medium">Due date week 8</span>

In this assignment, you need to work through your project and find any part of your project which does not need any process via server and could get the response or answer by calculate or process something by JavaScript. Implement required task that you found such as form validation. For any deletion, ask for confirmation before redirect to delete page, the confirmation should be done by using JavaScript. Sorting information that appear within a table should be done by using JavaScript.

## Lab Description

This exercise is individual work. The answer should be written as text
files with (.html) and (.js) extensions compressed together then submit
including student ID and Name as a comment in beginning of the JS file.

### Homework
Make sure you follow following file structure

```sh
id_first-name-cpit405-lab-assignment2    # Root folder
   - views                               # sub-folder
       - page1.html
       - page2.html
       - page3.html
       - ...
   - css                                 # sub-folder
       - style1.css
       - style2.css
       - ...
   - js                                  # sub-folder
       - script1.js
       - script2.js
       - ...
   - Documents                           # sub-folder
     - project-sketch.pdf                # Must be PDF format
     - project-description.pdf           # Must be PDF format
     
```

<span id="page2" class="anchor"></span>
