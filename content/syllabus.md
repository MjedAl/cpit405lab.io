---
title: "Syllabus"
date: 2018-08-25T14:09:23+03:00
draft: false
---

# CPIT-405 Internet Applications

# Course Syllabus

**Credits:** 3 credit hours
**Prerequisite:** CPIT-370 , CPIT-252

## Description
 
The objective of this course is to study Internet programming and Web application development. Students will learn basic principles and techniques for building Internet applications. It provides students with the basic Web page development technologies and an introduction to dynamic Web page development using client-side scripting. Topics include introduction to HTTP protocol and client side programming, XHTML, Cascading Style Sheets, JavaScript DOM and AJAX.

## Course Learning Outcomes
By completion of the course the students should be able to:

1. Describe the essential concepts associated with internet architecture that supports web applications.
2. Understand the basic structure of the World-Wide-Web.
3. Identify the compatibility issues between the well-known browsers.
4. Use HTML5 markup tags for structuring web pages.
5. Use HTML5 with appropriate CSS properties and elements for styling, formatting, and enhancing web pages.
6. Construct and validate web pages using HTML5 and
CSS3.
7. Implement client-side application logic using JavaScript.
8. Handle event using JavaScript and DOM in client-side.
9. Create and use extensible markup language.
10. Create JSON in JavaScript and insert JSON data into HTML.
11. Implement Server-Side script to serve client-side requests.
12. Develop dynamic web pages using Ajax technology.

## Textbook/References
- Paul J. Deitel, Harvey M. Deitel, Abbey Deitel, , "Internet and World Wide Web", Prentice Hall; 5 edition (2011-11) ISBN-13 9780132151009 ISBN-10 0132151006


## Topics
- The Internet and its Architecture
- Introduction to HTML5
- Cascading Style Sheets
- JavaScript: Part 1 (Introduction, Control Statements & Functions)
- JavaScript: Part 2 (Arrays & Objects)
- Document Object Model (DOM)
- JavaScript: Part 3 (Events)
- PHP and MySQL
- JavaScript Object Notation (JSON)
- Rich Internet Application Server Technologies


## Grading
- Labs: 15%
- Assignments: 10%
- Group Project: 20%
- Midterm exam: 25%
- Final exam: 30%
